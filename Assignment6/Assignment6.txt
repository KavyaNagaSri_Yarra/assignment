                     DAY 6 ASSIGNMENT

1)package assessment6;

import java.time.LocalTime;

class BasicThread extends Thread{
	public void run() {
		System.out.print("The default name ");
		System.out.println(Thread.currentThread().getName());
		Thread.currentThread().setName("t1");
		System.out.println(Thread.currentThread().getName());
		Thread.currentThread().setName("MyThread");
		System.out.println(Thread.currentThread().getName());
		System.out.println(LocalTime.now());
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println(LocalTime.now());
	}
}
public class ThreadDemo {
	public static void main(String[] args) {
		BasicThread thread=new BasicThread();
		thread.start();
	}

}


2)

Exception in thread "Thread-0" java.lang.Error: Unresolved compilation problem: 
	Unhandled exception type InterruptedException

        at assessment6.BasicThread.run(ThreadDemo.java:14)






3)
package assessment6;


	public class DemoThread1 implements Runnable{
		public void run() {
			for (int counter=1;counter<=10;counter++) {
				System.out.println("running "+Thread.currentThread().getName()+" in loop : "+counter);
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					
					e.printStackTrace();
				}
			}
		}
		public static void main(String[] args) {
			DemoThread1 dt1=new DemoThread1();
			Thread t1=new Thread(dt1);
			Thread t2=new Thread(dt1);
			Thread t3=new Thread(dt1);
			t1.start();
		      t2.start();
			t3.start();
		}
	}


4)
package assessment6;
public class DemoThread1 extends Thread {
	public DemoThread1(){
		start();
	}
	public void run() {
		for (int counter=1;counter<=10;counter++) {
			System.out.println("running "+Thread.currentThread().getName()+" in loop : "+counter);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				
				e.printStackTrace();
			}
		}
	}
	
	public static void main(String[] args) {
		new DemoThread1();
		new DemoThread1();
		new DemoThread1();
		
	}
}

5)

package assessment6;
class NumberDemo implements Runnable
{
   int n ;
  public NumberDemo(int n) 
   {
	   this.n=n;
   }
  public void run()
  {
     for(int i=1;i<=10;i++)
     {
    	 System.out.println(n*i);
     }
	
  }
 
}
public class Number
{
    public static void main(String args[]) 
    {
      NumberDemo thread1=new NumberDemo(2);
      NumberDemo thread2=new NumberDemo(5);
      NumberDemo thread3=new NumberDemo(8);
      Thread t1=new Thread(thread1);
      Thread t2=new Thread(thread2);
      Thread t3=new Thread(thread3);
      System.out.println("Thread 1 started");
      t1.start();
      try {
		t1.join();
	  } catch (InterruptedException e) {
		e.printStackTrace();
     	}
      System.out.println("Thread 1 Completed");
      
      
      System.out.println("Thread 2 started");
      t2.start();
      try {
		t2.join();
	  } catch (InterruptedException e) {
		e.printStackTrace();
     	}
      System.out.println("Thread 2 Completed");
      
      
      System.out.println("Thread 3 started");
      t3.start();
      try {
		t3.join();
	  } catch (InterruptedException e) {
		e.printStackTrace();
     	}
      System.out.println("Thread 3 Completed");
      
    }
}






