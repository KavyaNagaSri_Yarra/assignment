                       DAY3  ASSIGNMENT
1)


package assessment3;
import java.util.Scanner;
public class Lower {
public static void main(String args[]) {
	Scanner sc=new Scanner(System.in);
	String str=sc.nextLine();
	 String  LowerStr = str.toLowerCase();
	 System.out.println("The lower characters of string is:"+LowerStr);
	 sc.close();
	 
}
}
 
2)
package assessment3;
import java.util.Scanner;
public class Replace {
	public static void main(String args[]) {
		System.out.println("Enter the string:");
		Scanner sc=new Scanner(System.in);
		String str=sc.nextLine();
		String new_str=str.replace('d','h');
		System.out.println("the new string is:"+new_str);
		sc.close();
	}

}

4)

package assessment3;
import java.util.Scanner;

public class Search{

	public static int numberSearch(int[] arr,int x) {
		for(int i=0;i<arr.length;i++) {
			if(arr[i]==x)
				return i;
		}
		return -1;
	}
	public static void main(String[] args) {
		int[] arr= {3,5,7,1,9,2,6,8,4,0};
		Scanner sc=new Scanner(System.in);
		System.out.println("enter an element between 0 to 9-");
		int n=sc.nextInt();
		System.out.println("the element is at index-"+numberSearch(arr,n));
		sc.close();
	}
		

	}


5)

package assessment3;
import java.util.Scanner;
public class Substr {


public static void main(String[] args) {
	System.out.println("Enter the string:");
    Scanner sc = new Scanner(System.in);
    String S = sc.next();
   System.out.println("Enter the range:");
    int start = sc.nextInt();
    
    int end = sc.nextInt();
    
    System.out.println(S.substring(start,end));
    sc.close();
}
}

6)
package assessment3;
import java.util.Scanner;

public class Palindrome {
Scanner sc=new Scanner(System.in);

	static boolean isPalindrome(String str)
	{
		int i = 0, j = str.length() - 1;
	while (i < j) {
    if (str.charAt(i) != str.charAt(j))
	return false;
			i++;
			j--;
		}
   return true;
	}
	public static void main(String[] args)
	{
		Scanner sc=new Scanner(System.in);
		String str=sc.nextLine();
		sc.close();

		if (isPalindrome(str))
			System.out.print("Yes");
		else
			System.out.print("No");
	}
}

7)

package assessment3;
import java.util.Scanner;
public class Pangrams {
	 public static void main(String[] args) {
	      Scanner sc=new Scanner(System.in);
	      String str=sc.nextLine();
	      boolean[] alphaList = new boolean[26];
	      int index = 0;
	      int flag = 1;
	      for (int i = 0; i < str.length(); i++) {
	         if ( str.charAt(i) >= 'A' && str.charAt(i) <= 'Z') {
	            index = str.charAt(i) - 'A';
	         }else if( str.charAt(i) >= 'a' && str.charAt(i) <= 'z') {
	            index = str.charAt(i) - 'a';
	            sc.close();
	      }
	      alphaList[index] = true;
	   }
	   for (int i = 0; i <= 25; i++) {
	      if (alphaList[i] == false)
	      flag = 0;
	   }
	  
	   if (flag == 1)
	      System.out.print("The above string is a pangram.");
	   else
	      System.out.print("The above string is not a pangram.");
	   }
	}

8)
package assessment3;

import java.util.Scanner;
class UserMainCode8{
	public static String getString(String string) {
		int len = string.length();
		  String temp = "";
		  for (int i = 0; i < len; i++) 
		  {
		    if (i == 0 && string.charAt(i) == 'g')
		      temp += 'g';
		    else if (i == 1 && string.charAt(i) == 'h')
		      temp += 'h';
		    else if (i > 1)
		      temp += string.charAt(i);
		  }
		return temp;
		
	}
	
}

public class ModifiedString {
	

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
	      String str1 = sc.nextLine();
	      System.out.println("The given strings is: "+str1);
	      System.out.println("The new string is: "+UserMainCode8.getString(str1));
	      sc.close();

	}

}
9)

package assessment3;
class UserMainCode{
	public static String reShape(String str,char ch) {
		String res="";
		for(int i=str.length()-1;i>=0;i--)
				if(i==0) {
					res=res+str.charAt(i);
				}
				else {
					res=res+str.charAt(i)+ch;
				}
		return res;
	}
}
public class ReverseString{
	public static void main(String[] args) {
		String r="Rabbit";
		char c='-';
		System.out.println(UserMainCode.reShape(r, c));
}
}
 



