package com.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.model.Pet;
import com.model.User;

public class MyPetsDao implements DaoInterface{
	
	public boolean create(User user) {
		
		return false;
	}

	
	public boolean read(User user) {
		
		return false;
	}

	
	public boolean update(User user, int petId) {
		
		return false;
	}

	
	public boolean delete(String userId) {
		
		return false;
	}

	
	public String readLogin(User user) {
		
		return null;
	}

	
	public boolean savePet(Pet pet) {
		
		return false;
	}

	
	public List<Pet> readPet(User user) {
		PreparedStatement preparedStatement = null;
		String query="";
		ResultSet resultSet=null;
		List<Pet> petList=new ArrayList<Pet>();
		Connection connection=DBConnection.getConnection();
		try {
			query="SELECT * FROM PETS where PET_OWNERID=(SELECT ID FROM PET_USERS WHERE USER_NAME=?)";
			preparedStatement=connection.prepareStatement(query);
			preparedStatement.setString(1, user.getUserName());
			resultSet=preparedStatement.executeQuery();
			while(resultSet.next()){
				Pet pet=new Pet();
				pet.setPetId(resultSet.getInt("ID"));
				pet.setPetName(resultSet.getString("PET_NAME"));
				pet.setPetAge(resultSet.getInt("PET_AGE"));
				pet.setPetPlace(resultSet.getString("PET_PLACE"));
				pet.setOwnerId(resultSet.getInt("PET_OWNERID"));
				petList.add(pet);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return petList;
	}

	
	public List<Pet> readPet() {
		
		return null;
	}
	
}