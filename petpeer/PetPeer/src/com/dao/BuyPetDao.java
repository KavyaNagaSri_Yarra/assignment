package com.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import com.model.Pet;
import com.model.User;

public class BuyPetDao implements DaoInterface{
	
	public boolean create(User user) {
		
		return false;
	}

	
	public boolean read(User user) {
		
		return false;
	}

	
	public boolean update(User user,int petId) {
		PreparedStatement preparedStatement = null;
		String query=" ";
		Connection connection=DBConnection.getConnection();
		try {
			query="UPDATE PETS SET PET_OWNERID=(SELECT ID FROM PET_USERS WHERE USER_NAME=?) WHERE ID=?";
			preparedStatement=connection.prepareStatement(query);
			preparedStatement.setString(1, user.getUserName());
			preparedStatement.setInt(2,petId);
			preparedStatement.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return true;
	}

	
	public boolean delete(String userId) {
		
		return false;
	}

	
	public String readLogin(User user) {
		
		return null;
	}

	
	public boolean savePet(Pet pet) {
		
		return false;
	}

	
	public List<Pet> readPet() {
		
		return null;
	}

	
	public List<Pet> readPet(User user) {
		
		return null;
	}
	

}